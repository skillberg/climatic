package ru.cyber_eagle_owl.climaticsouls.ui.main.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.cyber_eagle_owl.climaticsouls.R;

public class DayWeatherFragment extends Fragment {

    public static DayWeatherFragment newInstance() {
        DayWeatherFragment dayWeatherFragment = new DayWeatherFragment();

        return dayWeatherFragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day_weather, container, false);

        return view;
    }
}
