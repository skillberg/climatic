package ru.cyber_eagle_owl.climaticsouls.ui.main;

import ru.cyber_eagle_owl.climaticsouls.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void startWeatherService();

    void checkAndRequestGeoPermission();
}
