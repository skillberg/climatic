package ru.cyber_eagle_owl.climaticsouls.ui.main;

import ru.cyber_eagle_owl.climaticsouls.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    Long getLastUpdate();

    void saveForecastToDb(String typeOfForecast, String forecastJson);
}
