package ru.cyber_eagle_owl.climaticsouls.ui.base;

/*It is an interface that is implemented by BasePresenter, it acts as base presenter interface that is extended by all other presenter interfaces.*/

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

}
