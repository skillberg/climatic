package ru.cyber_eagle_owl.climaticsouls.ui.base;

import android.support.v7.app.AppCompatActivity;

/*It is a base class for all activities, which implements MvpView and it is extended by all other activities in the application.*/
public class BaseActivity extends AppCompatActivity implements MvpView {
}
