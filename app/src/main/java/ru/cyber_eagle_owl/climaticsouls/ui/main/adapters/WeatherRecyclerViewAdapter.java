package ru.cyber_eagle_owl.climaticsouls.ui.main.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.cyber_eagle_owl.climaticsouls.R;

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder> {

    private static final int CURRENT_WEATHER_ITEM = 0;
    private static final int DAILY_WEATHER_ITEM = 1;
    private static final int CURRENT_WEATHER_ITEM_POSITION = 0;

    // В этом методе мы создаем новую ячейку
    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view;
        WeatherViewHolder weatherViewHolder;

        switch (viewType) {
            case CURRENT_WEATHER_ITEM:
                view = layoutInflater.inflate(R.layout.card_current_weather, parent, false);
                view.setTag(R.string.view_type_tag, CURRENT_WEATHER_ITEM);
                break;

            default:
                view = layoutInflater.inflate(R.layout.card_day_weather, parent, false);
                view.setTag(R.string.view_type_tag, DAILY_WEATHER_ITEM);
        }

        weatherViewHolder = new WeatherViewHolder(view);
        return weatherViewHolder;
    }

    // В этом методе мы привязываем данные к ячейке
    @Override
    public void onBindViewHolder(@NonNull final WeatherViewHolder holder, final int position) {

    }

    // В этом методе мы возвращаем количество элементов списка
    @Override
    public int getItemCount() {
        return 6;
    }

    @Override
    public int getItemViewType(final int position) {
        if (position == CURRENT_WEATHER_ITEM_POSITION) {
            return CURRENT_WEATHER_ITEM;
        } else {
            return DAILY_WEATHER_ITEM;
        }
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {
        public WeatherViewHolder(final View itemView) {
            super(itemView);
            switch ((int) itemView.getTag(R.string.view_type_tag)) {
                case CURRENT_WEATHER_ITEM:
                    //ищем элементы в layout card_current_weather
                    break;
                default:
                    //ищем элементы в layout card_day_weather
            }
        }
    }
}
