package ru.cyber_eagle_owl.climaticsouls.ui.main;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import javax.inject.Inject;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.dagger.StarterApplication;
import ru.cyber_eagle_owl.climaticsouls.dagger.components.ActivityComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.components.DaggerActivityComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ActivityModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.MainPresenterModule;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.current.CurrentWeather;
import ru.cyber_eagle_owl.climaticsouls.services.WeatherService;
import ru.cyber_eagle_owl.climaticsouls.ui.base.BaseActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.main.fragments.DayWeatherFragment;
import ru.cyber_eagle_owl.climaticsouls.ui.main.fragments.ListingFragment;

public class MainActivity extends BaseActivity implements MainMvpView {

    public static final String ACTION_GOT_WEATHER = "com.skillberg.weather2.ACTION_GOT_WEATHER";
    public static final String EXTRA_CURRENT_WEATHER = "current_weather";
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 0;
    private static final String TAG = "Weather/MainActivity";
    private static final String LISTING_FRAGMENT_TAG = "listFragment";
    private static final String DAY_WEATHER_FRAGMENT_TAG = "dayWeatherFragment";

    @Nullable
    private static boolean isTablet;

    private android.app.FragmentManager mFragmentManager;

    @Inject
    MainPresenter mPresenter;

    private final WeatherReceiver weatherReceiver = new WeatherReceiver();

    private ActivityComponent mActivityComponent;

    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .mainPresenterModule(new MainPresenterModule())
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(StarterApplication.get(this).getApplicationComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        isTablet = getResources().getBoolean(R.bool.isTablet);
        mFragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        if (mFragmentManager.findFragmentByTag(LISTING_FRAGMENT_TAG) == null &&
                mFragmentManager.findFragmentByTag(DAY_WEATHER_FRAGMENT_TAG) == null) {
            ListingFragment listingFragment = ListingFragment.newInstance();
            fragmentTransaction.add(R.id.mainFragmentContainer, listingFragment, LISTING_FRAGMENT_TAG);
            DayWeatherFragment dayWeatherFragment = DayWeatherFragment.newInstance();
            if (isTablet) {
                fragmentTransaction.add(R.id.dayWeatherFragmentContainer, dayWeatherFragment, DAY_WEATHER_FRAGMENT_TAG);
            }
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        //mPresenter = new MainPresenter(new DataManager(getSharedPreferences("weather", MODE_PRIVATE), this));
        mPresenter.onAttach(this);

        checkAndRequestGeoPermission();
        Log.i(TAG, "Last update: " + mPresenter.getLastUpdate());

        mPresenter.saveForecastToDb("Типа прогноз", "а тут должен быть охуенно длинный json");
    }

    @Override
    public void startWeatherService() {
        Intent intent = new Intent(this, WeatherService.class);
        startService(intent);
    }

    @Override
    public void checkAndRequestGeoPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION_PERMISSION);
        } else {
            startWeatherService();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startWeatherService();
            } else {
                // Нет гео
                // Попробуем показать ещё раз
                checkAndRequestGeoPermission();
            }
        }
    }

    private class WeatherReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_GOT_WEATHER.equals(intent.getAction())) {
                Log.i(TAG, "Got weather!");
                CurrentWeather currentWeather = intent.getParcelableExtra(EXTRA_CURRENT_WEATHER);

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter(ACTION_GOT_WEATHER);
        registerReceiver(weatherReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        unregisterReceiver(weatherReceiver);

        super.onStop();
    }
}
