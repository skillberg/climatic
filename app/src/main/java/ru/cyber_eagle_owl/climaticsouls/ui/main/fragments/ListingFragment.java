package ru.cyber_eagle_owl.climaticsouls.ui.main.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.ui.main.adapters.WeatherRecyclerViewAdapter;

public class ListingFragment extends Fragment {

    private RecyclerView weatherRecyclerView;
    private WeatherRecyclerViewAdapter mWeatherRecyclerViewAdapter;

    public static ListingFragment newInstance() {
        ListingFragment listingFragment = new ListingFragment();

        return listingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing, container, false);
        weatherRecyclerView = view.findViewById(R.id.weather_rv);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mWeatherRecyclerViewAdapter = new WeatherRecyclerViewAdapter();
        weatherRecyclerView.setAdapter(mWeatherRecyclerViewAdapter);
        mWeatherRecyclerViewAdapter.notifyDataSetChanged();

        return view;
    }
}
