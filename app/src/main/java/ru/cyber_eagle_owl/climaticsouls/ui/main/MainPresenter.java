package ru.cyber_eagle_owl.climaticsouls.ui.main;

import android.util.Log;

import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.ui.base.BasePresenter;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    private DataManager mDataManager;
    private static final String TAG = "Weather/MainPresenter";

    public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public Long getLastUpdate() {
        return mDataManager.getLastUpdate();
    }

    @Override
    public void saveForecastToDb(final String typeOfForecast, final String forecastJson) {
        Log.i(TAG, String.valueOf(mDataManager.insert(typeOfForecast, forecastJson)) + " type: " + typeOfForecast + " json: " + forecastJson);
    }
}
