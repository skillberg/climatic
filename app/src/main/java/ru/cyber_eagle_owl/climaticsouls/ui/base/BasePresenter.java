package ru.cyber_eagle_owl.climaticsouls.ui.base;

/*It is base class for all presenter that implements MvpPresenter and it is extended by all other presenters there in application.*/
public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    protected V mMvpView;

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    public V getMvpView() {
        return mMvpView;
    }
}