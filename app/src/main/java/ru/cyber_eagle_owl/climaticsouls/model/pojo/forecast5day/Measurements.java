
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Measurements implements Parcelable {

    @SerializedName("dt")
    @Expose
    private Long mForecastDataUnixStyle;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("weather")
    @Expose
    private java.util.List<Weather> weather = null;
    @SerializedName("clouds")
    @Expose
    private Clouds clouds;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("rain")
    @Expose
    private Rain rain;
    @SerializedName("snow")
    @Expose
    private Snow snow;
    public final static Parcelable.Creator<Measurements> CREATOR = new Creator<Measurements>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Measurements createFromParcel(Parcel in) {
            return new Measurements(in);
        }

        public Measurements[] newArray(int size) {
            return (new Measurements[size]);
        }

    }
    ;

    protected Measurements(Parcel in) {
        this.mForecastDataUnixStyle = ((Long) in.readValue((Long.class.getClassLoader())));
        this.main = ((Main) in.readValue((Main.class.getClassLoader())));
        in.readList(this.weather, (Weather.class.getClassLoader()));
        this.clouds = ((Clouds) in.readValue((Clouds.class.getClassLoader())));
        this.wind = ((Wind) in.readValue((Wind.class.getClassLoader())));
        this.rain = ((Rain) in.readValue((Rain.class.getClassLoader())));
        this.snow = ((Snow) in.readValue((Snow.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Measurements() {
    }

    /**
     *
     * @param mForecastDataUnixStyle Time of data forecasted, unix, UTC
     * @param main Object, which provides temperature, atmospheric pressure and humidity, %
     * @param weather Object, which provides weather description (Rain, Snow, Extreme etc.)
     * @param clouds Object, which provides cloudiness, %
     * @param wind Object, which provides speed and direction of wind
     * @param rain Object, which provides rain volume for last 3 hours, mm
     * @param snow Object, which provides snow volume for last 3 hours
     */
    public Measurements(Long mForecastDataUnixStyle, Main main, java.util.List<Weather> weather, Clouds clouds, Wind wind, Rain rain, Snow snow) {
        super();
        this.mForecastDataUnixStyle = mForecastDataUnixStyle;
        this.main = main;
        this.weather = weather;
        this.clouds = clouds;
        this.wind = wind;
        this.rain = rain;
        this.snow = snow;
    }

    public Long getForecastDataUnixStyle() {
        return mForecastDataUnixStyle;
    }

    public void setForecastDataUnixStyle(Long forecastDataUnixStyle) {
        this.mForecastDataUnixStyle = forecastDataUnixStyle;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public java.util.List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Snow getSnow() {
        return snow;
    }

    public void setSnow(Snow snow) {
        this.snow = snow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mForecastDataUnixStyle", mForecastDataUnixStyle).append("main", main).append("weather", weather).append("clouds", clouds).append("wind", wind).append("rain", rain).append("snow", snow).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mForecastDataUnixStyle);
        dest.writeValue(main);
        dest.writeList(weather);
        dest.writeValue(clouds);
        dest.writeValue(wind);
        dest.writeValue(rain);
        dest.writeValue(snow);
    }

    public int describeContents() {
        return  0;
    }

}
