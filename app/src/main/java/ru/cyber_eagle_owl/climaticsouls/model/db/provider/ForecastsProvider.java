package ru.cyber_eagle_owl.climaticsouls.model.db.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class ForecastsProvider extends ContentProvider {

    private ForecastsDbHelper mForecastsDbHelper;

    private static final int FORECAST = 1;
    private static final int FORECASTS = 2;

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
    URI_MATCHER.addURI(ForecastContract.AUTHORITY, "forecasts/#", FORECAST);
    URI_MATCHER.addURI(ForecastContract.AUTHORITY, "forecasts", FORECASTS);
    }

    @Override
    public boolean onCreate() {
        mForecastsDbHelper = new ForecastsDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull final Uri uri, @Nullable final String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable final String sortOrder) {
        SQLiteDatabase db = mForecastsDbHelper.getReadableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case FORECAST:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = ForecastContract.Forecasts._ID + " = ?";
                    selectionArgs = new String[]{id};
                } else {
                    selection = selection + " AND " + ForecastContract.Forecasts._ID + " = ?";
                    String[] newSelectionArgs = new String[selectionArgs.length + 1];
                    System.arraycopy(selectionArgs, 0, newSelectionArgs, 0, selectionArgs.length);
                    newSelectionArgs[newSelectionArgs.length - 1] = id;
                    selectionArgs = newSelectionArgs;
                }
                return db.query(ForecastContract.Forecasts.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull final Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case FORECAST:
                return ForecastContract.Forecasts.URI_TYPE_FORECAST_ITEM;
            case FORECASTS:
                return ForecastContract.Forecasts.URI_TYPE_FORECAST_DIR;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull final Uri uri, @Nullable final ContentValues values) {
        SQLiteDatabase db = mForecastsDbHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case FORECASTS:
                long rowId = db.insert(ForecastContract.Forecasts.TABLE_NAME,
                        null,
                        values);
                if (rowId > 0) {
                    Uri tempUri = ContentUris.withAppendedId(ForecastContract.Forecasts.URI, rowId);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return tempUri;
                }
                return null;
            default:
            return null;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable final String selection, @Nullable final String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull final Uri uri, @Nullable final ContentValues values, @Nullable final String selection, @Nullable final String[] selectionArgs) {
        return 0;
    }
}
