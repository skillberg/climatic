
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class FiveDayForecast implements Parcelable {

    @SerializedName("cnt")
    @Expose
    private Integer mMeasurementsCount;
    @SerializedName("list")
    @Expose
    private java.util.List<Measurements> mMeasurementsList = null;
    public final static Parcelable.Creator<FiveDayForecast> CREATOR = new Creator<FiveDayForecast>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FiveDayForecast createFromParcel(Parcel in) {
            return new FiveDayForecast(in);
        }

        public FiveDayForecast[] newArray(int size) {
            return (new FiveDayForecast[size]);
        }

    }
    ;

    protected FiveDayForecast(Parcel in) {
        this.mMeasurementsCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.mMeasurementsList, (Measurements.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public FiveDayForecast() {
    }

    /**
     *
     * @param mMeasurementsCount Number of measurementsList returned by API call
     * @param mMeasurementsList List of measurementsList returned by API call
     */
    public FiveDayForecast(Integer mMeasurementsCount, java.util.List<Measurements> mMeasurementsList) {
        super();
        this.mMeasurementsCount = mMeasurementsCount;
        this.mMeasurementsList = mMeasurementsList;
    }

    public Integer getMeasurementsCount() {
        return mMeasurementsCount;
    }

    public void setMeasurementsCount(Integer measurementsCount) {
        this.mMeasurementsCount = measurementsCount;
    }

    public java.util.List<Measurements> getMeasurementsList() {
        return mMeasurementsList;
    }

    public void setMeasurementsList(java.util.List<Measurements> measurementsList) {
        this.mMeasurementsList = measurementsList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mMeasurementsCount", mMeasurementsCount).append("mMeasurementsList", mMeasurementsList).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mMeasurementsCount);
        dest.writeList(mMeasurementsList);
    }

    public int describeContents() {
        return  0;
    }

}
