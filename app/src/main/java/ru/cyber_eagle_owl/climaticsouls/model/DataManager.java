package ru.cyber_eagle_owl.climaticsouls.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.ApplicationContext;
import ru.cyber_eagle_owl.climaticsouls.model.api.ApiHelper;
import ru.cyber_eagle_owl.climaticsouls.model.db.DbHelper;
import ru.cyber_eagle_owl.climaticsouls.model.db.provider.ForecastContract;
import ru.cyber_eagle_owl.climaticsouls.model.preferences.SharedPreferencesHelper;

public class DataManager {
    private ApiHelper apiHelper;
    private SharedPreferencesHelper mPreferencesHelper;
    private DbHelper mDbHelper;

    public DataManager(SharedPreferences mSharedPreferences, @ApplicationContext Context mMvpView) {
        this.apiHelper = new ApiHelper();
        this.mPreferencesHelper = new SharedPreferencesHelper(mSharedPreferences);
        this.mDbHelper = new DbHelper(mMvpView);
    }

    public DbHelper getDbHelper() {
        return mDbHelper;
    }

    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    public SharedPreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public Long getLastUpdate() {
        return mPreferencesHelper.getLastUpdate();
    }

    public Uri insert(final String typeOfForecast, final String forecastJson) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ForecastContract.Forecasts.COLUMN_TYPE_OF_FORECAST, typeOfForecast);
        contentValues.put(ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST, System.currentTimeMillis());
        contentValues.put(ForecastContract.Forecasts.COLUMN_FORECAST_JSON, forecastJson);
        return mDbHelper.insert(ForecastContract.Forecasts.URI, contentValues);
    }
}
