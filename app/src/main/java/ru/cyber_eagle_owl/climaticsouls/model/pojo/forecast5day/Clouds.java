
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Clouds implements Parcelable
{

    @SerializedName("all")
    @Expose
    private Integer cloudsPercentage;
    public final static Parcelable.Creator<Clouds> CREATOR = new Creator<Clouds>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Clouds createFromParcel(Parcel in) {
            return new Clouds(in);
        }

        public Clouds[] newArray(int size) {
            return (new Clouds[size]);
        }

    }
    ;

    protected Clouds(Parcel in) {
        this.cloudsPercentage = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Clouds() {
    }

    /**
     * 
     * @param cloudsPercentage Cloudiness, %
     */
    public Clouds(Integer cloudsPercentage) {
        super();
        this.cloudsPercentage = cloudsPercentage;
    }

    public Integer getCloudsPercentage() {
        return cloudsPercentage;
    }

    public void setCloudsPercentage(Integer cloudsPercentage) {
        this.cloudsPercentage = cloudsPercentage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cloudsPercentage", cloudsPercentage).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cloudsPercentage);
    }

    public int describeContents() {
        return  0;
    }

}
