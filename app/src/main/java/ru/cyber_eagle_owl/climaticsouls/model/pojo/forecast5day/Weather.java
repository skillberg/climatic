
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Weather implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("main")
    @Expose
    private String groupOfWeatherParameters;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("icon")
    @Expose
    private String icon;
    public final static Parcelable.Creator<Weather> CREATOR = new Creator<Weather>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        public Weather[] newArray(int size) {
            return (new Weather[size]);
        }

    }
    ;

    protected Weather(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.groupOfWeatherParameters = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.icon = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Weather() {
    }

    /**
     * 
     * @param id Weather condition id
     * @param groupOfWeatherParameters Group of weather parameters (Rain, Snow, Extreme etc.)
     * @param description Weather condition within the group
     * @param icon Weather icon id
     */
    public Weather(Integer id, String groupOfWeatherParameters, String description, String icon) {
        super();
        this.id = id;
        this.groupOfWeatherParameters = groupOfWeatherParameters;
        this.description = description;
        this.icon = icon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupOfWeatherParameters() {
        return groupOfWeatherParameters;
    }

    public void setGroupOfWeatherParameters(String groupOfWeatherParameters) {
        this.groupOfWeatherParameters = groupOfWeatherParameters;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("groupOfWeatherParameters", groupOfWeatherParameters).append("description", description).append("icon", icon).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(groupOfWeatherParameters);
        dest.writeValue(description);
        dest.writeValue(icon);
    }

    public int describeContents() {
        return  0;
    }

}
