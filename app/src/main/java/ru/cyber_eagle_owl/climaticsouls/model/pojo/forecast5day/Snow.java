
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Snow implements Parcelable
{

    @SerializedName("3h")
    @Expose
    private Double volumeForThreeHours;
    public final static Parcelable.Creator<Snow> CREATOR = new Creator<Snow>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Snow createFromParcel(Parcel in) {
            return new Snow(in);
        }

        public Snow[] newArray(int size) {
            return (new Snow[size]);
        }

    }
    ;

    protected Snow(Parcel in) {
        this.volumeForThreeHours = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Snow() {
    }

    /**
     * 
     * @param volumeForThreeHours Snow volume for last 3 hours
     */
    public Snow(Double volumeForThreeHours) {
        super();
        this.volumeForThreeHours = volumeForThreeHours;
    }

    public Double getVolumeForThreeHours() {
        return volumeForThreeHours;
    }

    public void setVolumeForThreeHours(Double volumeForThreeHours) {
        this.volumeForThreeHours = volumeForThreeHours;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("volumeForThreeHours", volumeForThreeHours).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(volumeForThreeHours);
    }

    public int describeContents() {
        return  0;
    }

}
