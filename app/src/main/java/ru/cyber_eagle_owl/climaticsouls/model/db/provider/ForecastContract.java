package ru.cyber_eagle_owl.climaticsouls.model.db.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public final class ForecastContract {

    public static final String DB_NAME = "forecasts.db";
    public static final int DB_VERSION = 1;

    public static final String AUTHORITY = "ru.cyber_eagle_owl.climaticsouls.provider";
    public static final String URI = "content://" + AUTHORITY;

    public static final String[] CREATE_DATABASE_QUERIES = {
            Forecasts.CREATE_TABLE
    };
    private ForecastContract() {
    }

    public static abstract class Forecasts implements BaseColumns {

        //самодельный MIME тип
        public static final String URI_TYPE_FORECAST_ITEM = "vnd.android.cursor.item/vnd.ru.cyber_eagle_owl.forecast";
        public static final String URI_TYPE_FORECAST_DIR = "vnd.android.cursor.dir/vnd.ru.cyber_eagle_owl.forecast";

        public static final String TABLE_NAME = "forecasts";
        public static final Uri URI = Uri.parse(ForecastContract.URI + "/" + TABLE_NAME);

        public static final String COLUMN_TYPE_OF_FORECAST = "type_of_forecast";
        public static final String COLUMN_DATE_OF_FORECAST = "date_of_forecast";
        public static final String COLUMN_FORECAST_JSON = "forecast_json";

        public static final String CREATE_TABLE = String.format("CREATE TABLE %s " +
                        "(%s INTEGER PRIMARY KEY, " +
                        "%s TEXT NOT NULL, " +
                        "%s INTEGER NOT NULL, " +
                        "%s TEXT NOT NULL);",
                TABLE_NAME,
                _ID,
                COLUMN_TYPE_OF_FORECAST,
                COLUMN_DATE_OF_FORECAST,
                COLUMN_FORECAST_JSON);
    }

}
