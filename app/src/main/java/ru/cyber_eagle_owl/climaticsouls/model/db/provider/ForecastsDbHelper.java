package ru.cyber_eagle_owl.climaticsouls.model.db.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ForecastsDbHelper extends SQLiteOpenHelper {

    public ForecastsDbHelper(Context context) {
        super(context, ForecastContract.DB_NAME, null, ForecastContract.DB_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        for (String query : ForecastContract.CREATE_DATABASE_QUERIES) {
            sqLiteDatabase.execSQL(query);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {

    }
}
