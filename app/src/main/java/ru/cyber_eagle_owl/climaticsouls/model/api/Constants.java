package ru.cyber_eagle_owl.climaticsouls.model.api;

public class Constants {

    public static final String API_BASE_URL = "https://api.openweathermap.org/data/2.5/";

    public static final String API_KEY = "3ae85f0e32712fae84e5b09227409b0b"; // test_key_1 https://home.openweathermap.org/api_keys

    public static final String DEFAULT_UNITS = "metric";

}