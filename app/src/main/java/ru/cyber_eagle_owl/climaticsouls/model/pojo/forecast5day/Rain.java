
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Rain implements Parcelable
{

    @SerializedName("3h")
    @Expose
    private Double volumeForThreeHours;
    public final static Parcelable.Creator<Rain> CREATOR = new Creator<Rain>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Rain createFromParcel(Parcel in) {
            return new Rain(in);
        }

        public Rain[] newArray(int size) {
            return (new Rain[size]);
        }

    }
    ;

    protected Rain(Parcel in) {
        this.volumeForThreeHours = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Rain() {
    }

    /**
     * 
     * @param volumeForThreeHours Rain volume for last 3 hours, mm
     */
    public Rain(Double volumeForThreeHours) {
        super();
        this.volumeForThreeHours = volumeForThreeHours;
    }

    public Double getVolumeForThreeHours() {
        return volumeForThreeHours;
    }

    public void setVolumeForThreeHours(Double volumeForThreeHours) {
        this.volumeForThreeHours = volumeForThreeHours;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("volumeForThreeHours", volumeForThreeHours).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(volumeForThreeHours);
    }

    public int describeContents() {
        return  0;
    }

}
