package ru.cyber_eagle_owl.climaticsouls.dagger.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.ApplicationContext;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;

import static android.content.Context.MODE_PRIVATE;

@Module
public class DataManagerModule {

    @Provides
    @Singleton
    @NonNull
    public DataManager provideDataManager(@ApplicationContext Context mMvpView) {
        return new DataManager(mMvpView.getSharedPreferences("weather", MODE_PRIVATE), mMvpView);
    }
}
