package ru.cyber_eagle_owl.climaticsouls.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.cyber_eagle_owl.climaticsouls.dagger.StarterApplication;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ApplicationModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.DataManagerModule;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;

@Singleton
@Component(modules = {ApplicationModule.class, DataManagerModule.class})
public interface ApplicationComponent {

    DataManager getDataManager();

    void inject(StarterApplication application);

}
