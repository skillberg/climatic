package ru.cyber_eagle_owl.climaticsouls.dagger.components;

import dagger.Component;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.PerActivity;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ActivityModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.MainPresenterModule;
import ru.cyber_eagle_owl.climaticsouls.ui.main.MainActivity;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, MainPresenterModule.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
